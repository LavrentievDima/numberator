(function ($) {
    $.fn.numberator = function (options) {
        let settings = $.extend({
            'width': '118px',
            'height': '38px',
            'button-width': '38px',
            'background-color': '#1E90FF',
            'color': 'white',
            'border': 'none',
            'border-radius': '0',
            'margin-left': '0',
            'margin-right': '0',
            'round-btn': false,
        }, options);
        return this.each((i, el) => {

            $(el).wrap('<div class="numberator_wrapper">');
            let parent = $(el).parent();
            parent.prepend('<button class="numberator__btn numberator__btn-decrement btn-decrement" type="button"><strong>-</strong></button>');
            parent.append('<button class="numberator__btn numberator__btn-increment btn-increment" type="button"><strong>+</strong></button>');
            $(el).addClass('numberator__input');
            let input = parent.find('.numberator__input');
            let btn = parent.find('.numberator__btn');
            let btn_decrement = parent.find('.numberator__btn-decrement');
            let btn_increment = parent.find('.numberator__btn-increment');

            let input_width = parseInt(settings['width'].split('px')[0]) - parseInt(settings['button-width'].split('px')[0]) * 2;

            parent.css({width: settings['width']});
            btn.css({
                width: settings['button-width'],
                height: settings['height'],
                verticalAlign: 'top',
                backgroundColor: settings['background-color'],
                color: settings['color'],
                border: 'none',
                borderTop: settings['border'],
                borderBottom: settings['border'],
            });
            btn_decrement.css({
                borderLeft: settings['border'],
                borderRadius: `${settings['border-radius']} 0 0 ${settings['border-radius']}`,
                marginRight: `${settings['margin-right']}`
            });
            btn_increment.css({
                borderRight: settings['border'],
                borderRadius: `0 ${settings['border-radius']} ${settings['border-radius']} 0`,
                marginLeft: `${settings['margin-left']}`
            });
            $(el).css({
                width: input_width,
                height: settings['height'],
                verticalAlign: 'top',
                textAlign: 'center',
                border: 'none',
                borderTop: settings['border'],
                borderBottom: settings['border'],
                borderLeft: settings['border'],
                borderRight: settings['border'],
            });

            if (settings['round-btn']) {
                btn_decrement.css({
                    borderRadius: '50%',
                });
                btn_increment.css({
                    borderRadius: '50%',
                });
                $(el).css({
                    border: settings['border'],
                    width: input_width - 6,
                    margin: '0 3px',
                });
            }

            $(document).on('hover', '.numberator__btn', function (e) {

                btn.css({

                    backgroundColor: 'red',

                });
            });

            $(document).on('click', '.numberator__btn-decrement', function (e) {

                // console.log($(e.target));
                let inp = $(e.target).closest('.numberator_wrapper').find('.numberator__input');
                inp.val(parseInt(inp.val()) - 1);
            });

            $(document).on('click', '.numberator__btn-increment', function (e) {

                // console.log($(e.target));
                let inp = $(e.target).closest('.numberator_wrapper').find('.numberator__input');
                inp.val(parseInt(inp.val()) + 1);
            });


            input.on('focus', () => {

            });

            input.on('blur', (e) => {

            });

            input.on('keyup', (e) => {
                switch (e.keyCode) {
                    case 13:
                        //Enter

                        break;
                    case 27:
                        //Esc
                        input.val('1');
                        break;
                    case 38:
                        //KeyUp

                        break;
                    case 40:
                        //KeyDown

                        break;
                    default:

                        break;
                }

            });
            input.on('keydown', (e) => {

                switch (e.keyCode) {
                    case 13:
                        //Enter
                        break;
                    case 27:
                        //Esc
                        break;
                    case 38:
                        //KeyUp


                        break;
                    case 40:
                        //KeyDown


                        break;
                    default:
                        break;
                }
            });
        });
    };
})(jQuery);
